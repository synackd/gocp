package main

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	flag "github.com/spf13/pflag"
	"gitlab.com/synackd/gocp/copy"
)

var (
	// All the flags in one place
	flags struct {
		recursive           bool
		ask                 bool
		force               bool
		followSymlinksSet   bool
		noFollowSymlinksSet bool
		followSymlinks      bool
		verbose             bool
		version             bool
	}
	stdin    = bufio.NewReader(os.Stdin)
	baseName = "gocp"
	version  = "undefined"
)

// init initializes the flags and usage message.
func init() {
	setUsage()
	setFlagDefaults()
}

// setFlagDefaults initiates the flags structure with
// the default values and sets the usage message.
func setFlagDefaults() {
	flag.BoolP("help", "h", false, "print this help and exit")
	flag.BoolVarP(&flags.recursive, "recursive", "r", false, "copy directories recursively")
	flag.BoolVarP(&flags.recursive, "RECURSIVE", "R", false, "same as -r")
	flag.BoolVarP(&flags.ask, "interactive", "i", false, "prompt before overwrite (overrides -n)")
	flag.BoolVarP(&flags.force, "force", "f", false, "if destination exists, remove it and try again (ignored when -n used)")
	flag.BoolVarP(&flags.followSymlinksSet, "dereference", "L", false, "always follow symbolic links in SOURCE")
	flag.BoolVarP(&flags.noFollowSymlinksSet, "no-dereference", "P", false, "never follow symbolic links in SOURCE (overrides -L)")
	flag.BoolVarP(&flags.verbose, "verbose", "v", false, "print to stdout the files being copied")
	flag.BoolVarP(&flags.version, "version", "V", false, "output version information and exit")
}

// resetFlagDefaults repopulates the flags structure with
// the default values.
func resetFlagDefaults() {
	flags.recursive = false
	flags.ask = false
	flags.force = false
	flags.followSymlinks = true
	flags.verbose = false
}

// setUsage sets the usage message.
func setUsage() {
	flag.Usage = func() {
		baseName = filepath.Base(os.Args[0])
		fmt.Fprintf(os.Stderr, "Usage: %s [OPTION]... SOURCE DEST\n"+
			"   or: %s [OPTION]... SOURCE... DIRECTORY\n"+
			"Copy SOURCE to DEST, or multiple SOURCE(s) to DIRECTORY.\n\n"+
			"OPTIONS:\n", baseName, baseName)
		flag.PrintDefaults()
	}
}

// printVersion prints a version string for --version.
func printVersion() {
	fmt.Printf("gocp %s\n", version)
	fmt.Printf("Written by Devon Bautista\n")
}

// errorf is a wrapper that prints an error message on stderr
func errorf(format string, a ...interface{}) {
	if !strings.HasSuffix(format, "\n") {
		format += "\n"
	}
	_, e := fmt.Fprintf(os.Stderr, baseName+": "+format, a...)
	if e != nil {
		panic(e)
	}
}

// promptOverwrite asks the user if they want to overwrite the
// file at path. If anything besides 'y' is entered,
// promptOverwrite will return false.
func promptOverwrite(path string) (bool, error) {
	fmt.Printf(baseName+": overwrite %q?: ", path)
	answer, err := stdin.ReadString('\n')
	if err != nil {
		return false, err
	}
	if strings.ToLower(answer)[0] != 'y' {
		return false, nil
	}
	return true, nil
}

// checkFiles is meant to be called on each file being copied
// to check:
//
// (1) src, if a dir, is not copied without -r.
// (2) src and dst are not the same file.
// (3) the user is prompted to overwrite an existing dst if -i
//     was passed and -f was not.
func checkFiles(src, dst string, srcInfo os.FileInfo) error {
	// (1)
	if !flags.recursive && srcInfo.IsDir() {
		errorf("-r not specified; omitting directory %q", src)
		return copy.NewErrorSkip(src)
	}

	// (3) Part 1
	dstInfo, err := os.Stat(dst)
	if err != nil && !os.IsNotExist(err) {
		errorf("%q: cannot handle error: %v", dst, err)
		return copy.NewErrorSkip(src)
	} else if err != nil {
		// dst does not exist
		return nil
	}
	// dst exists

	// (2)
	if os.SameFile(srcInfo, dstInfo) {
		errorf("%q and %q are the same file", src, dst)
		return copy.NewErrorSkip(src)
	}

	// (3) Part 2
	if flags.ask && !flags.force {
		overwrite, err := promptOverwrite(dst)
		if err != nil {
			return err
		}
		if !overwrite {
			return copy.NewErrorSkip(src)
		}
	}

	return nil
}

// copyUsingArgs uses args to configure copy options
// and copy passed file(s).
func copyUsingArgs(args []string) error {
	// Get source(s) and destination
	src := args[:len(args)-1]
	dst := args[len(args)-1]
	toDir := false

	// Determine if dst is a directory
	dstInfo, err := os.Stat(dst)
	if err == nil {
		toDir = dstInfo.IsDir()
	}
	if len(args) > 2 && !toDir {
		err = fmt.Errorf("target %q is not a directory", dst)
		errorf(err.Error())
		return err
	}

	// Option configuration
	opts := copy.GetDefaultOptions()
	opts.Recursive = flags.recursive
	opts.Clobber = flags.force
	opts.FollowSymlinks = flags.followSymlinks
	opts.PreHook = checkFiles
	opts.PostHook = func(src, dst string) {
		if flags.verbose {
			fmt.Printf("%q -> %q\n", src, dst)
		}
	}

	// Copy each file
	var lastError error
	for _, file := range src {
		to := dst
		if toDir {
			to = filepath.Join(to, filepath.Base(file))
		}
		if err := opts.Copy(file, to); err != nil {
			errorf("%v", err)
			lastError = err
		}
	}
	return lastError
}

func main() {
	// Deal with flags, leaving args
	flag.Parse()

	// Print version and exit if -V or --version
	if flags.version {
		printVersion()
		os.Exit(0)
	}

	// Make sure there are >= 2 args
	if flag.NArg() < 2 {
		flag.Usage()
		os.Exit(1)
	}

	// Decide whether to follow symlinks
	flags.followSymlinks = (flags.followSymlinksSet && !flags.noFollowSymlinksSet) ||
		(!flags.followSymlinksSet && !flags.noFollowSymlinksSet)

	// Perform copying using args
	if err := copyUsingArgs(flag.Args()); err != nil {
		os.Exit(1)
	}
}
