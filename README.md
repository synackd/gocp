# gocp

A copy tool and library, written in Go.

This project is meant to provide many features of the Linux `cp` command as a library, as well as a command-line tool which uses said library under the hood.

# Library Usage
```go
import (
    cp "gitlab.com/synackd/gocp/copy"
)

func main() {
    // Generate an Options struct that would be equivalent to
    // calling cp without any flags
    opts := cp.GetDefaultOptions()

    // Tweak options
    opts.Clobber = false

    // Copy files, directories, or symlinks
    opts.Copy("/path/to/src", "/path/to/dst")

    // OR

    // Simply copy items using default configuration (like calling
    // cp without any flags)
    cp.Copy("/path/to/src", "/path/to/dst")
}
```

# Tool Usage
```
Usage: gocp [OPTION]... SOURCE DEST
   or: gocp [OPTION]... SOURCE... DIRECTORY
Copy SOURCE to DEST, or multiple SOURCE(s) to DIRECTORY.
```
See [Tool](#tool) for options.

# Progress
## Library
The currently supported options are:
<table>
<thead>
  <tr>
    <th>Option</th>
    <th>Description</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>Recursive</td>
    <td>copy directories recursively (default: false)</td>
  </tr>
  <tr>
    <td>Clobber</td>
    <td>overwrite existing files (default: true)</td>
  </tr>
  <tr>
    <td>FollowSymlinks</td>
    <td>copy targets instead of symbolic links itself (default: true)</td>
  </tr>
  <tr>
    <td>Verbose</td>
    <td>print type and file(s) being copied (default: false)</td>
  </tr>
  <tr>
    <td>PreHook<br></td>
    <td>a pre-copy hook called on each file to be copied (default: nil)</td>
  </tr>
  <tr>
    <td>PostHook</td>
    <td>a post-copy hook called after each file is copied (default: nil)</td>
  </tr>
</tbody>
</table>

## Tool
The currently supported options are:
```
-L,     --dereference      always follow symbolic links in SOURCE
-f,     --force            if destination exists, remove it and try again (ignored when -n used)
-i,     --interactive      prompt before overwrite (overrides -n)
-P,     --no-dereference   never follow symbolic links in SOURCE (overrides -L)
-r, -R, --recursive        copy directories recursively
-v,     --verbose          print to stdout the files being copied
```
