VERSION := $(shell git describe --always --dirty --long)
OUT := gocp
PKG := gitlab.com/synackd/gocp

all: build

build:
	go build -v -o "${OUT}" -ldflags="-X main.version=$$(git describe --always --dirty --long)"

clean:
	@go clean

test:
	@go test ${PKG}
	@go test ${PKG}/copy
