package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"strings"
	"testing"

	"gitlab.com/synackd/gocp/copy"
	"gitlab.com/synackd/gocp/copy/cmp"
	"gitlab.com/synackd/gocp/testlib"
)

// copyAndTestStderr runs copyUsingArgs with args and compares
// its output on stderr with expected. If expected is not contained
// in stderr, testing fails.
func copyAndTestStderr(t *testing.T, args []string, expected string) {
	// Temporarily redirect stderr to buffer
	oldStderr := os.Stderr
	r, w, err := os.Pipe()
	if err != nil {
		t.Fatalf("Error creating pipe: %v", err)
	}
	os.Stderr = w

	// Attempt copying, with stderr being piped
	copyUsingArgs(args)

	// Copy stderr output to buffer for comparison
	// (through channel using goroutine to prevent
	// indefinite blocking)
	chanOut := make(chan string)
	go func() {
		var outBuf bytes.Buffer
		io.Copy(&outBuf, r)
		chanOut <- outBuf.String()
	}()

	// Restore stderr and compare output
	w.Close()
	os.Stderr = oldStderr
	out := <-chanOut
	if !strings.Contains(out, expected) {
		t.Fatalf("Copy(%q -> %q) = %v, want %v", args[0], args[1], out, nil)
	}
}

// TestCpFile tests copying a regular file with default
// options. It is equivalent to:
//  cp src dst
// on the command line.
func TestCpFile(t *testing.T) {
	// Create src file
	f, err := testlib.RandomFile("", "testfile-")
	if err != nil {
		t.Fatalf("Unable to create random file: %v", err)
	}
	fPath := f.Name()
	defer os.RemoveAll(fPath)

	// Copy to dst
	fPathCopy := fPath + "-copy"
	if err = copyUsingArgs([]string{fPath, fPathCopy}); err != nil {
		t.Fatalf("Copy(%q -> %q) = %v, want %v", fPath, fPathCopy, err, nil)
	}
	defer os.RemoveAll(fPathCopy)

	// Check if they are equal
	defaultOpts := copy.GetDefaultOptions()
	if err = cmp.IsEqualFile(defaultOpts, fPath, fPathCopy); err != nil {
		t.Fatalf("Copy(%q -> %q) = files are different: %v", fPath, fPathCopy, err)
	}
}

// TestCpDirectory tests copying a directory with default
// options (i.e. no -r for copying directory contents).
// It is equivalent to:
//  cp src_dir dst_dir
// on the command line.
func TestCpDirectory(t *testing.T) {
	// Create src dir
	d, err := testlib.RandomTree("", "test_dir-", testlib.MaxDirDepth)
	if err != nil {
		t.Fatalf("Unable to create random directory tree: %v", err)
	}
	defer os.RemoveAll(d)
	dCopy := d + "-copy"

	// Test copying
	expected := fmt.Sprintf("-r not specified; omitting directory %q", d)
	copyAndTestStderr(t, []string{d, dCopy}, expected)
}

// TestCpRecursive tests the recursive flag by copying
// directories in the case where the destination exists
// and that where it does not.
// The equivalent is:
//  cp -r src_dir dst_dir
// on the command line.
func TestCpRecursive(t *testing.T) {
	// Recursion on
	flags.recursive = true
	defer resetFlagDefaults()
	opts := copy.GetDefaultOptions()
	opts.Recursive = true

	// Create tmpdir for testing
	var tmpDir, srcDir, dstDir, dstDirExists string
	var err error
	tmpDir, err = ioutil.TempDir("", "test_recursive-")
	if err != nil {
		t.Fatalf("Unable to create temp dir: %v", err)
	}
	defer os.RemoveAll(tmpDir)

	// Create paths for src dir, and dst dir that already exists
	srcDir = path.Join(tmpDir, "test_src")
	err = os.Mkdir(srcDir, 0755)
	if err != nil {
		t.Fatalf("Unable to create dir %q: %v", srcDir, err)
	}
	if _, err := testlib.RandomFile(srcDir, "test_file-"); err != nil {
		t.Fatalf("Unable to create random file: %v", err)
	}
	dstDir = path.Join(tmpDir, "test_dst")
	dstDirExists = dstDir + "_exists"
	if err := os.Mkdir(dstDirExists, 0755); err != nil {
		t.Fatalf("Unable to create dir %q: %v", dstDirExists, err)
	}

	t.Run("existing_dst_dir", func(t *testing.T) {
		if err := copyUsingArgs([]string{srcDir, dstDirExists}); err != nil {
			t.Fatalf("Copy(%q -> %q) = %v, want %v", srcDir, dstDirExists, err, nil)
		}

		// Since dst exists, copied dir should be inside dstDirExists
		realDst := path.Join(dstDirExists, path.Base(srcDir))
		if err := cmp.IsEqualDir(opts, srcDir, realDst); err != nil {
			t.Fatalf("Copy(%q -> %q) = file trees not equal: %v", srcDir, realDst, err)
		}
	})

	t.Run("nonexisting_dst_dir", func(t *testing.T) {
		if err := copyUsingArgs([]string{srcDir, dstDir}); err != nil {
			t.Fatalf("Copy(%q -> %q) = %v, want %v", srcDir, dstDir, err, nil)
		}
		if err := cmp.IsEqualDir(opts, srcDir, dstDir); err != nil {
			t.Fatalf("Copy(%q -> %q) = file trees not equal: %v", srcDir, dstDir, err)
		}
	})
}

// TestCpRecursiveMultiple tests copying multiple source dirs
// into a single destination dir.
func TestCpRecursiveMultiple(t *testing.T) {
	// Recursion on
	flags.recursive = true
	defer resetFlagDefaults()
	opts := copy.GetDefaultOptions()
	opts.Recursive = true

	// Create tmpdir for testing
	tmpDir, err := ioutil.TempDir("", "test_recursive_multi-")
	if err != nil {
		t.Fatalf("Unable to create temp dir: %v", err)
	}
	defer os.RemoveAll(tmpDir)

	// Create source dirs
	srcDirs := []string{}
	for i := 0; i < testlib.MaxDirDepth; i++ {
		dirPath := path.Join(tmpDir, fmt.Sprintf("src_dir-%d", i))
		if err := os.Mkdir(dirPath, 0755); err != nil {
			t.Fatalf("Unable to create dir %q: %v", dirPath, err)
		}
		if _, err := testlib.RandomFile(dirPath, "test_file-"); err != nil {
			t.Fatalf("Unable top create ramdom file: %v", err)
		}
		srcDirs = append(srcDirs, dirPath)
	}

	// Test copying when dst does not exist
	dstDir := path.Join(tmpDir, "dst")
	args := append(srcDirs, dstDir)
	expected := fmt.Sprintf("target %q is not a directory", dstDir)
	copyAndTestStderr(t, args, expected)

	// Test copying when dst exists
	if err := os.Mkdir(dstDir, 0755); err != nil {
		t.Fatalf("Unable to create dir %q: %v", dstDir, err)
	}
	if err := copyUsingArgs(args); err != nil {
		t.Fatalf("copy %q returned %v, want %v", args, err, nil)
	}

	// Is each copied dir in dst equal to that in src?
	for _, src := range srcDirs {
		dst := path.Join(dstDir, path.Base(src))
		if err := cmp.IsEqualDir(opts, src, dst); err != nil {
			t.Fatalf("Copy(%q -> %q) = file trees not equal: %v", src, dst, err)
		}
	}
}

// TestCpSymlink tests following symlinks (default):
//  cp -L symlink target-copy
// and copying the link itself:
//  cp -P symlink symlink-copy
func TestCpSymlink(t *testing.T) {
	// Create temporary directory to work in
	tmpDir, err := ioutil.TempDir("", "test_symlinks-")
	if err != nil {
		t.Fatalf("Unable to create temp dir: %v", err)
	}
	defer os.RemoveAll(tmpDir)

	// Create target file
	targ, err := testlib.RandomFile(tmpDir, "test_file-")
	if err != nil {
		t.Fatalf("Unable to create random file: %v", err)
	}
	defer targ.Close()
	targPath := targ.Name()

	// Create symlink to target
	linkPath := path.Join(tmpDir, "link")
	if err := os.Symlink(targPath, linkPath); err != nil {
		t.Fatalf("Unable to create symlink: %v", err)
	}

	t.Run("no-follow-symlinks", func(t *testing.T) {
		defer resetFlagDefaults()
		flags.followSymlinks = false
		opts := copy.GetDefaultOptions()
		opts.FollowSymlinks = false

		linkCopyPath := linkPath + "-copy"
		if err := copyUsingArgs([]string{linkPath, linkCopyPath}); err != nil {
			t.Fatalf("Copy(%q -> %q) = %v, want %v", linkPath, linkCopyPath, err, nil)
		}
		if err := cmp.IsEqualFile(opts, linkPath, linkCopyPath); err != nil {
			t.Fatalf("Copy(%q -> %q) = files not equal: %v", linkPath, linkCopyPath, err)
		}
	})

	t.Run("follow-symlinks", func(t *testing.T) {
		defer resetFlagDefaults()
		flags.followSymlinks = true
		opts := copy.GetDefaultOptions()
		opts.FollowSymlinks = true

		targCopyPath := targPath + "-copy"
		if err := copyUsingArgs([]string{linkPath, targCopyPath}); err != nil {
			t.Fatalf("Copy(%q -> %q) = %v, want %v", linkPath, targCopyPath, err, nil)
		}
		if err := cmp.IsEqualFile(opts, targPath, targCopyPath); err != nil {
			t.Fatalf("Copy(%q -> %q) = files not equal: %v", targPath, targCopyPath, err)
		}
	})
}
