// Package testlib provides common routines for testing.
package testlib

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"path"
)

const (
	MaxFileSize = 1000 // In bytes
	MaxDirDepth = 5    // For generating test trees
	MaxNumFiles = 5    // Per directory
)

// RandomFile creates a random file like os.TempFile, but also adds
// random content.
func RandomFile(path, prefix string) (*os.File, error) {
	f, err := ioutil.TempFile(path, prefix)
	if err != nil {
		return nil, err
	}

	var bytes = []byte{}
	for i := 0; i < rand.Intn(MaxFileSize); i++ {
		bytes = append(bytes, byte(i))
	}
	f.Write(bytes)

	return f, nil
}

// RandomTree creates a file tree (directory) of a certain depth
// containing a number of files (within the maximum), each
// containing random content.
func RandomTree(root, prefix string, depth int) (string, error) {
	// Create first subdirectory
	rootPath, err := ioutil.TempDir(root, prefix+"0_")
	if err != nil {
		return "", err
	}

	// Create the rest of the subdirectories
	currDir := path.Join(root, rootPath)
	for currDepth := 1; currDepth < depth; currDepth++ {
		// Create next directory
		newDir, err := ioutil.TempDir(currDir, fmt.Sprintf(prefix+"%d_", currDepth))
		if err != nil {
			return "", err
		}

		// Create files with random content in each level
		for i := 0; i < MaxNumFiles; i++ {
			_, err := RandomFile(currDir, fmt.Sprintf(prefix+"%d_", currDepth))
			if err != nil {
				return "", err
			}
		}

		currDir = newDir
	}
	return rootPath, nil
}
