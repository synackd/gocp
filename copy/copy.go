// Package copy implements utilities for copying regular
// files, directories, and symlinks.
package copy

// Copy uses the default options to copy
// src to dst.
func Copy(src, dst string) error {
	var defaultOpts = GetDefaultOptions()
	return defaultOpts.Copy(src, dst)
}
