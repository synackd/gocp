package copy_test

import (
	"io/ioutil"
	"os"
	"path"
	"regexp"
	"testing"

	"gitlab.com/synackd/gocp/copy"
	"gitlab.com/synackd/gocp/copy/cmp"
	"gitlab.com/synackd/gocp/testlib"
)

// copyFileAndTest is a wrapper that performs Copy() with the passed
// options and compares the result with the source file using
// cmp.IsEqualFile().
func copyFileAndTest(t *testing.T, opt copy.Options, src, dst string) {
	if err := opt.Copy(src, dst); err != nil {
		t.Fatalf("Copy(%q -> %q) = %v, want %v", src, dst, err, nil)
	}
	if err := cmp.IsEqualFile(opt, src, dst); err != nil {
		t.Fatalf("Expected %q and %q to be the same, got %v", src, dst, err)
	}
}

// copyDirAndTest is a wrapper that performs Copy() with the passed
// options and compares the result with the source directory using
// cmp.IsEqualTree().
func copyDirAndTest(t *testing.T, opt copy.Options, src, dst string) {
	if err := opt.Copy(src, dst); err != nil {
		t.Fatalf("Copy(%q -> %q) = %v, want %v", src, dst, err, nil)
	}
	if err := cmp.IsEqualDir(opt, src, dst); err != nil {
		t.Fatalf("Expected %q and %q to be the same, got %v", src, dst, err)
	}
}

// TestCopyFile tests copying a regular file.
func TestCopyFile(t *testing.T) {
	f, err := testlib.RandomFile("", "testfile-")
	if err != nil {
		t.Fatalf("Unable to create temp file: %v", err)
	}
	fName := f.Name()
	defer os.RemoveAll(fName)

	fNameCopy := fName + "-copy"
	defaultOpts := copy.GetDefaultOptions()
	copyFileAndTest(t, defaultOpts, fName, fNameCopy)
	defer os.RemoveAll(fNameCopy)
}

// TestCopyDir tests copying a directory with
// and without Recursive option set.
func TestCopyDir(t *testing.T) {
	// Create source directory
	dPath, err := testlib.RandomTree("", "test_dir-", testlib.MaxDirDepth)
	if err != nil {
		t.Fatalf("Unable to generate random directory tree: %v", err)
	}
	defer os.RemoveAll(dPath)

	// Test shallow copy
	NoRecursive := copy.Options{
		Recursive: false,
	}
	dPathCopyShallow := dPath + "-copy-shallow"
	copyDirAndTest(t, NoRecursive, dPath, dPathCopyShallow)
	defer os.RemoveAll(dPathCopyShallow)

	// Test deep copy
	Recursive := copy.Options{
		Recursive: true,
	}
	dPathCopyDeep := dPath + "-copy-deep"
	copyDirAndTest(t, Recursive, dPath, dPathCopyDeep)
	defer os.RemoveAll(dPathCopyDeep)
}

// TestCopySymlink tests copying a symlink, both the symlink itself
// and the file it points to based on the FollowSymlinks option.
func TestCopySymlink(t *testing.T) {
	// Create configs
	noFollowSymlink := copy.Options{
		FollowSymlinks: false,
	}
	followSymlink := copy.Options{
		FollowSymlinks: true,
	}

	tmpPath := os.TempDir()

	// Create target file
	targ, err := ioutil.TempFile("", "testtarget-")
	if err != nil {
		t.Fatalf("Unable to create temp file: %v", err)
	}
	targPath := targ.Name()
	defer os.RemoveAll(targPath)

	// Create symlink to target
	symName := "testsym-" + path.Base(targPath)
	symPath := path.Join(tmpPath, symName)
	err = os.Symlink(targPath, symPath)
	if err != nil {
		t.Fatalf("Unable to create symlink: %v", err)
	}
	defer os.RemoveAll(symPath)

	// Test copying symlink itself
	symNameCopy := symName + "-copy"
	symPathCopy := path.Join(tmpPath, symNameCopy)
	copyFileAndTest(t, noFollowSymlink, symPath, symPathCopy)
	defer os.RemoveAll(symPathCopy)

	// Test copying symlink target
	targPathCopy := targPath + "-copy"
	copyFileAndTest(t, followSymlink, symPath, targPathCopy)
	defer os.RemoveAll(targPathCopy)
}

// TestClobber tests overwriting a file/directory/symlink.
func TestClobber(t *testing.T) {
	// Create config
	clobber := copy.Options{
		Clobber: true,
	}

	tmpPath := os.TempDir()

	// Regular file
	f, err := testlib.RandomFile("", "testfile-")
	if err != nil {
		t.Fatalf("Unable to create random file: %v", err)
	}
	fPath := f.Name()
	defer os.RemoveAll(fPath)
	fPathCopy := fPath + "-copy"
	copyFileAndTest(t, clobber, fPath, fPathCopy)
	defer os.RemoveAll(fPathCopy)
	copyFileAndTest(t, clobber, fPath, fPathCopy)

	// Directory
	d, err := testlib.RandomTree("", "test_clobber-", testlib.MaxDirDepth)
	if err != nil {
		t.Fatalf("Unable to generate random directory tree: %v", err)
	}
	defer os.RemoveAll(d)
	dCopy := d + "-copy"

	clobber.Recursive = false
	copyDirAndTest(t, clobber, d, dCopy)
	defer os.RemoveAll(dCopy)
	copyDirAndTest(t, clobber, d, dCopy)

	clobber.Recursive = true
	copyDirAndTest(t, clobber, d, dCopy)
	copyDirAndTest(t, clobber, d, dCopy)

	// Symlink
	symName := "testsym-" + path.Base(fPath)
	symPath := path.Join(tmpPath, symName)
	err = os.Symlink(fPath, symPath)
	if err != nil {
		t.Fatalf("Unable to create symlink: %v", err)
	}
	defer os.RemoveAll(symPath)

	symPathCopy := symPath + "-copy"

	clobber.FollowSymlinks = false
	copyFileAndTest(t, clobber, symPath, symPathCopy)
	defer os.RemoveAll(symPathCopy)
	copyFileAndTest(t, clobber, symPath, symPathCopy)

	clobber.FollowSymlinks = true
	copyFileAndTest(t, clobber, symPath, fPathCopy)
	copyFileAndTest(t, clobber, symPath, fPathCopy)
}

// TestSkip tests skipping a file using a pre-copy
// and post-copy hook.
func TestSkip(t *testing.T) {
	fPrefix := "testfile-"
	fCopySuffix := "-copy"
	fpr := regexp.MustCompile(`.*` + fPrefix + `.*`)
	fsr := regexp.MustCompile(`.*` + fPrefix + `[a-zA-Z0-9]+` + fCopySuffix)

	// Define hooks
	skip := copy.GetDefaultOptions()
	skip.PreHook = func(src, dst string, srcInfo os.FileInfo) error {
		if fpr.MatchString(src) {
			return &copy.ErrorSkip{
				File: src,
			}
		}
		return nil
	}
	skip.PostHook = func(src, dst string) {
		// Check if file got skipped
		if fsr.MatchString(dst) {
			if _, err := os.Stat(dst); err == nil {
				t.Fatalf("file %q exists (%q should have been skipped)", dst, src)
				os.RemoveAll(dst)
			}
		}
	}

	// Create file
	f, err := testlib.RandomFile("", fPrefix)
	if err != nil {
		t.Fatalf("Unable to create random file: %v", err)
	}
	fName := f.Name()
	defer os.RemoveAll(fName)

	fNameCopy := fName + "-copy"
	if err := skip.Copy(fName, fNameCopy); err != nil {
		t.Fatalf("Copy(%q -> %q) = %v, want %v", fName, fNameCopy, err, nil)
	}
}
